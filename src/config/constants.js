export const CALCULATOR_OPERATORS = {
    add: '+',
    subtract: '-',
    multiply: 'x',
    divide: '÷',
};
export const WEBSOCKET_URL = 'ws://echo.websocket.org/';
export const COMMENTS = [
    {
        id: 0,
        text: 'Lorem ipsum dolor sit amet',
    }, {
        id: 1,
        text: 'Cum an quidam civibus molestie',
    }, {
        id: 2,
        text: 'Mei no vero laboramus',
    }, {
        id: 3,
        text: 'Case habeo modus vis ut',
    }, {
        id: 4,
        text: 'Eu vel timeam adolescens',
    }, {
        id: 5,
        text: 'In apeirian omittantur pri',
    }, {
        id: 6,
        text: 'No eos clita oporteat',
    }, {
        id: 7,
        text: 'Ei vidit essent definitiones pri',
    }, {
        id: 8,
        text: 'Cu nam salutandi efficiendi',
    }, {
        id: 9,
        text: 'Duo pericula erroribus consulatu te',
    },
];
