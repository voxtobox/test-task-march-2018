import Vue from 'vue';
import { CALCULATOR_OPERATORS } from '@/config/constants';

export default class FractionItemData {
    /**
     * Constructor
     * @param num
     * @param den
     * @param operator
     */
    constructor(num = null, den = null, operator = 'add') {
        // Numerator
        this._num = num;
        // Denominator
        this._den = den;
        // Operator
        this._operator = operator;
    }

    get num() {
        return this._num;
    }

    get den() {
        return this._den;
    }

    get operator() {
        return this._operator;
    }

    /**
     * Is Numerator error
     * Useful to show that inout is error
     * @returns {boolean}
     */
    get isNumError() {
        return this.num === 0 && this.operator === 'divide';
    }

    /**
     * Is Numerator invalid
     * Do not calculate if true
     * @returns {boolean}
     */
    get isNumInvalid() {
        return this.num === undefined || this.num === null || this.isNumError;
    }

    /**
     * Is Denominator error
     * Useful to show that inout is error
     * @returns {boolean}
     */
    get isDenError() {
        return this.den === 0;
    }

    /**
     * Is Denominator is invalid
     * Do not calculate if true
     * @returns {boolean}
     */
    get isDenInvalid() {
        return !this.den;
    }

    /**
     * Is Fraction is invalid
     * Do not calculate if true
     * @returns {boolean}
     */
    get isInvalid() {
        return this.isNumInvalid || this.isDenInvalid;
    }

    /**
     * Is fraction has a negative sign
     * @returns {boolean}
     */
    get isNegative() {
        return this.num < 0 || this.den < 0;
    }

    /**
     * If fraction has priority operator such as multiply or divide
     * @returns {boolean}
     */
    get isPriorityOperator() {
        return this.operator === 'multiply' || this.operator === 'divide';
    }

    /**
     * Set numerator reactivity
     * @param num
     */
    set num(num) {
        Vue.set(this, '_num', num);
    }

    /**
     * Set denominator reactivity
     * @param den
     */
    set den(den) {
        Vue.set(this, '_den', den);
    }

    /**
     * Set operator reactivity
     * check if operator is valid
     * @param operator
     */
    set operator(operator) {
        if (CALCULATOR_OPERATORS[operator]) {
            Vue.set(this, '_operator', operator);
        } else {
            // eslint-disable-next-line no-console
            console.error(`${operator} is a wrong operator`);
        }
    }

    /**
     * Find greatest common divisor by algorithm Euclidean
      */
    _getGcd(a, b) {
        return b === 0 ? a : this._getGcd(b, a % b);
    }

    /**
     * Create new instance with same values
     * @returns {FractionItemData}
     */
    clone() {
        return new FractionItemData(this.num, this.den, this._operator);
    }

    /**
     * Add some fraction to this
     * @param fraction
     */
    add(fraction) {
        this.num = (this._num * fraction.den) + (this._den * fraction.num);
        this.den = (this._den * fraction.den);
        this.reducing();
    }

    /**
     * Subtract from this with some fraction
     * @param fraction
     */
    subtract(fraction) {
        this.num = (this.num * fraction.den) - (this.den * fraction.num);
        this.den = this.den * fraction.den;
        this.reducing();
    }

    /**
     * Multiply some fraction on this fraction
     * @param fraction
     */
    multiply(fraction) {
        this.num = this.num * fraction.num;
        this.den = this.den * fraction.den;
        this.reducing();
    }

    /**
     * divide this with some fraction
     * @param fraction
     */
    divide(fraction) {
        this.num = this.num * fraction.den;
        this.den = this.den * fraction.num;
        this.reducing();
    }

    /**
     * Just some sugar to call tight operator
     * @param fraction
     */
    calculate(fraction) {
        this[fraction.operator](fraction);
    }

    /**
     * Find greatest common divisor and divide my values to reduce fraction
     */
    reducing() {
        const gcd = this._getGcd(this.num, this.den);
        this.num = this.num / gcd;
        this.den = this.den / gcd;
    }
}
