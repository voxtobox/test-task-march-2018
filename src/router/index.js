import Vue from 'vue';
import Router from 'vue-router';

const FractionCalculator = () => import('@/components/FractionCalculator/FractionCalculator');
const WebSocketHandler = () => import('@/components/WebSocketHandler/WebSocketHandler');
const Home = () => import('@/components/Home');

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            name: 'Home',
            component: Home,
        }, {
            path: '/calculator',
            name: 'FractionCalculator',
            component: FractionCalculator,
        }, {
            path: '/websocket-handler',
            name: 'WebSocketHandler',
            component: WebSocketHandler,
        },
    ],
});
